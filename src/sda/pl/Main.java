package sda.pl;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class Main extends Application {

    private final BorderPane layout = new BorderPane();
    private final WebView browser = new WebView();
    private final WebEngine engine = browser.getEngine();
    private final HBox addressBar = new HBox(10);

    private Button backButton = new Button("<<");
    private Button refreshButton = new Button("@");
    private TextField addressField = new TextField();

    private TabPane tabs = new TabPane();


    public static void main(final String... args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        engine.load("http://google.com");

        //TOP
        addressBar.setPadding(new Insets(10, 10, 10, 10));
        addressBar.setAlignment(Pos.CENTER);
        backButton.setAlignment(Pos.CENTER_LEFT);
        addressField.setAlignment(Pos.CENTER);
        addressField.setMinWidth(500);
        refreshButton.setAlignment(Pos.CENTER_RIGHT);

        addressBar.getChildren().add(backButton);
        addressBar.getChildren().add(addressField);
        addressBar.getChildren().add(refreshButton);
        //---

        //CENTER
        Tab mainTab = new Tab("Welcome");
        Tab newTab = new Tab("...");
        mainTab.setContent(browser);
        tabs.getTabs().add(mainTab);
        tabs.getTabs().add(newTab);
        //---

        //LEFT
        TreeItem<String> historyItem = new TreeItem<>("History");
        historyItem.setExpanded(true);
        TreeItem<String> viewingItem = new TreeItem<>("Today");
        historyItem.getChildren().add(viewingItem);
        TreeView history = new TreeView<>(historyItem);
        StackPane root = new StackPane();
        root.getChildren().add(history);
        //---

        layout.setCenter(tabs);
        layout.setTop(addressBar);
        layout.setLeft(root);

        primaryStage.setTitle("JavaFX Browser");
        primaryStage.setScene(new Scene(layout, layout.getMaxWidth(), layout.getMaxHeight()));
        primaryStage.show();
    }

}
